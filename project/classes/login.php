<?php
session_start();
if (isset($_POST['submit'])) {
    $email = $_POST['email'];
    $pass = $_POST['pass'];
    require "../connection.php";
    $selectQuery = "select * from users where email = '$email' limit 1";
    // echo json_encode(['error'=>false,'message'=>$selectQuery]) ;
    // exit;
    $r = $conn->query($selectQuery);
    if ($r->num_rows > 0) {
        $user = $r->fetch_assoc();
        if (password_verify($pass, $user['pass'])) {
            $_SESSION['loggedIn'] = true;
            $_SESSION['name'] = $user['name'];
            $_SESSION['email'] = $user['email'];
            echo json_encode(['error' => false, 'message' => "Login successful"]);
        } else {
            echo json_encode(['error' => true, 'message' => "Invalid password"]);
        }
    } else {
        echo json_encode(['error' => true, 'message' => "Invalid email"]);
    }
}