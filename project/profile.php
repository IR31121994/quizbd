<?php
session_start();
require 'connection.php';

require "inc/header.php";
?>
    <h1 id="title2">My Profile</h1>
    <hr>
    <button id="copyBtn" class="btn btn-primary">Get paragraph 1 value</button>
    <hr>
    <textarea name="" id="paracontainer" class="form-control"></textarea>


    <section>

<div class="container">
  <strong class="title">My Profile</strong>
</div>

<div class="profile-box box-left">

  <?php
    
    $query = "SELECT * FROM users WHERE name = '".$_SESSION['name']."'";

    ;

    if($result = mysqli_query($conn, $query)) {

      $row = mysqli_fetch_assoc($result);

      echo "<div class='info'><strong>User No:</strong> <span>".$row['id']."</span></div>";
      echo "<div class='info'><strong>User Name:</strong> <span>".$row['name']."</span></div>";

    } else {
      die("Error with the query in the database");
    }

  ?>

  <div class="options">
    <a class="btn btn-primary" href="editprofile.php">Edit Profile</a>
    <a class="btn btn-success" href="changepassword.php">Change Password</a>
  </div>

</div>

</section>

<?php
require "inc/footer.php";
?>